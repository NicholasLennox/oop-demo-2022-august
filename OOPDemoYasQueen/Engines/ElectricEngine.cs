﻿using OOPDemoYasQueen.Bicycles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPDemoYasQueen.Engines
{
    internal class ElectricEngine : IEngine
    {
        public void AddFuel(double fuel)
        {
            Console.WriteLine("Charging the capacitors," + fuel);
        }

        public double GetFuelLevel()
        {
            return 0;
        }

        public void UseFuel()
        {
            Console.WriteLine("Bzz bzz");
        }
    }
}
