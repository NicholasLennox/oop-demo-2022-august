﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPDemoYasQueen.Engines
{
    internal class PetrolEngine : IEngine
    {
        public void AddFuel(double fuel)
        {
            Console.WriteLine("Adding fuel: " + fuel);
        }

        public double GetFuelLevel()
        {
            return 100;
        }

        public void UseFuel()
        {
            Console.WriteLine("Hungry hungry hippo");
        }
    }
}
