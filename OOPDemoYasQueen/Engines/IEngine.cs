﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPDemoYasQueen.Engines
{
    internal interface IEngine
    {
        double GetFuelLevel();
        void AddFuel(double fuel);
        void UseFuel();
    }
}
