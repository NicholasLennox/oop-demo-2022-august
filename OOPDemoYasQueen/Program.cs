﻿using OOPDemoYasQueen.Bicycles;
using System.Data;

namespace OOPDemoYasQueen
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello, World!");
            BmxBike bmx = new BmxBike("Tony Hawk", 100, "All");
            MountainBike mountain = new MountainBike("Trek", 1000, "10x1");
            mountain.PrintDetails();
            bmx.MakeNoise();
            DoNoise(bmx);
            DoNoise(mountain);
            Drive(bmx);
        }

        public static void DoNoise(Bicycle bike)
        {
            bike.MakeNoise();
        }

        public static void Drive(IDrivable driver)
        {
            driver.Accelerate();
            driver.Decelerate();
        }
    }
}