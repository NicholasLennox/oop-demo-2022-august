﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPDemoYasQueen.Bicycles
{
    internal interface IDrivable
    {
        void Accelerate();
        void Decelerate();
    }
}
