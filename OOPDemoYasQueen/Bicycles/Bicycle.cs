﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPDemoYasQueen.Bicycles
{
    internal abstract class Bicycle : IDrivable
    {
        public string Brand { get; init; }
        public int Riders { get; init; }
        public double Price { get; init; }

        public Bicycle(string brand, int riders, double price)
        {
            Brand = brand;
            Riders = riders;
            Price = price;
        }

        public virtual void MakeNoise()
        {
            Console.WriteLine("Tring tring");
        }

        public abstract void PrintDetails();

        public virtual void Accelerate()
        {
            Console.WriteLine("Im going faster");
        }

        public virtual void Decelerate()
        {
            Console.WriteLine("Im slowing down");
        }
    }
}
