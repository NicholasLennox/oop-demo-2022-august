﻿using OOPDemoYasQueen.Engines;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPDemoYasQueen.Bicycles
{
    internal class EBike : Bicycle
    {
        public IEngine Engine { get; init; }
        public EBike(string brand, int riders, double price, IEngine engine) : base(brand, riders, price)
        {
            Engine = engine;
        }

        public override void PrintDetails()
        {
            Console.WriteLine("I like peddling, but not really.");
        }

        public override void Accelerate()
        {
            base.Accelerate();
            Engine.UseFuel();
        }

        public override void Decelerate()
        {
            base.Decelerate();
            Engine.AddFuel(199);
        }
    }
}
