﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPDemoYasQueen.Bicycles
{
    internal class MountainBike : Bicycle
    {
        public string Gearing { get; init; }
        public MountainBike(string brand, double price, string gearing ) : base(brand, 1, price)
        {
            Gearing = gearing;
        }

        public override void PrintDetails()
        {
            Console.WriteLine("Hey man, want to go get lost in the forest, duuude.");
        }
    }
}
